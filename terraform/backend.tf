### Configure the backend
terraform {
  backend "s3" {
    bucket  = "lemur-infra"
    key     = "Terraform/backend/prod/lemur-ec2/terraform.tfstate"
    region  = "us-west-2"
  }
}