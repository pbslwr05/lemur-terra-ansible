provider "aws" {
  region = "us-west-2"
}

resource "aws_instance" "lemur" {
  count = "${var.instance_count["lemur"]}"

  ami                         = "${var.ami}"
  instance_type               = "${var.instance_type}"
  key_name                    = "${var.key_name}"
  vpc_security_group_ids      = ["${aws_security_group.lemur_sg.id}"]
  associate_public_ip_address = "${var.associate_public_ip_address}"
  ebs_optimized               = "${var.ebs_optimized}"
  disable_api_termination     = "${var.disable_api_termination}"
  subnet_id                   = "${var.subnet_id}"
  user_data                   = "${base64encode(file("${path.module}/mount-lemur.sh"))}"

  tags = {
        Name = "lemur-${var.instance_prefix}-${format("%02d", count.index+1)}"
    }

  root_block_device {
    volume_type           = "${var.root_volume_type}"
    volume_size           = "${var.root_volume_size}"

    }

  ebs_block_device{
      device_name = "/dev/sdb"
      volume_size = 50
      volume_type = "gp2"
    }
}

resource "aws_network_interface" "lemur" {
  count = "${var.instance_count["lemur"]}"
  subnet_id       = "${var.secondary_subnet}"
  security_groups = ["${aws_security_group.lemur_sg.id}"]
  attachment {
        instance = "${aws_instance.lemur[count.index].id}"
        device_index = 1
    }
}